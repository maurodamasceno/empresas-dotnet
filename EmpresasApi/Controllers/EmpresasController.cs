using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EmpresasApi.Models;
using Microsoft.EntityFrameworkCore;

namespace EmpresasApi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class EmpresasController : ControllerBase
  {
    private readonly EmpresaContext _context;

    public EmpresasController(EmpresaContext context)
    {
      _context = context;
    }

    //GET:      api/empresas
    [HttpGet]
    public ActionResult<IEnumerable<Empresa>> GetEmpresaItens()
    {
      return _context.EmpresaItens;
    }

    //GET:      api/empresas/n
    [HttpGet("{id}")]
    public ActionResult<Empresa> GetEmpresaItem(int id)
    {
      var empresaItem = _context.EmpresaItens.Find(id);

      if (empresaItem == null)
      {
        return NotFound();
      }

      return empresaItem;
    }

    //POST:     api/empresas
    [HttpPost]
    public ActionResult<Empresa> PostEmpresaItem(Empresa empresa)
    {
      _context.EmpresaItens.Add(empresa);
      _context.SaveChanges();

      return CreatedAtAction("GetEmpresaItem", new Empresa { id = empresa.id }, empresa);
    }

    //PUT:      api/empresas/n
    [HttpPut("{id}")]
    public ActionResult PutEmpresaItem(int id, Empresa empresa)
    {
      if (id != empresa.id)
      {
        return BadRequest();
      }

      _context.Entry(empresa).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    //DELETE:   api/empresas/n
    [HttpDelete("{id}")]
    public ActionResult<Empresa> DeleteEmpresaItem(int id)
    {
      var empresaItem = _context.EmpresaItens.Find(id);

      if (empresaItem == null)
      {
        return NotFound();
      }

      _context.EmpresaItens.Remove(empresaItem);
      _context.SaveChanges();

      return empresaItem;
    }
  }
}