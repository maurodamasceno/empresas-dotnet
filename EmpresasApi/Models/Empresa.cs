namespace EmpresasApi.Models
{
  public class Empresa
  {
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public string order { get; set; }
    public string folders { get; set; }
    public int owner { get; set; }
  }
}