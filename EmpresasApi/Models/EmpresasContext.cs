using Microsoft.EntityFrameworkCore;

namespace EmpresasApi.Models
{
  public class EmpresaContext : DbContext
  {
    public EmpresaContext(DbContextOptions<EmpresaContext> options) : base(options)
    {

    }
    public DbSet<Empresa> EmpresaItens { get; set; }
  }
}