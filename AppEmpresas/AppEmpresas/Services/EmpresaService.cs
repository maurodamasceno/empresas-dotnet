﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using AppEmpresas.Models;

namespace AppEmpresas.Services
{
    public class EmpresaService : IEmpresaService
    {
        public async Task<bool> AddEmpresasAsync(Empresa novaEmpresa)
        {
            novaEmpresa.Id = Guid.NewGuid();
            _context.Empresas.Add(novaEmpresa);

            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 1;
        }

        /*
        public Task<Empresa[]> AddEmpresasAsync(ApplicationUser user)
        {
            // hardcoded apenas para testar as respostas.
            var empresa1 = new Empresa
            {
                Name = "ioasys"
            };
            var empresa2 = new Empresa
            {
                Name = "Microsoft"
            };

            return Task.FromResult(new[] { empresa1, empresa2 });
        }
        */
    }
}
