﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using AppEmpresas.Models;

namespace AppEmpresas.Services
{
    public interface IEmpresaService
    {
        Task<Empresa[]> AddEmpresasAsync(
            ApplicationUser user);
    }
}
