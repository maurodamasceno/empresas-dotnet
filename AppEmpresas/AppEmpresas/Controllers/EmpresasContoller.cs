﻿using AppEmpresas.Models;
using AppEmpresas.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AppEmpresas.Controllers
{
    [Authorize]
    public class EmpresasContoller : Controller
    {
        private readonly IEmpresaService _empresaService;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmpresasContoller(IEmpresaService empresaService,
            UserManager<ApplicationUser> userManager)
        {
            _empresaService = empresaService;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser == null) return Challenge();

            var empresas = await _empresaService.GetEmpresasAsync(currentUser);

            var model = new EmpresaViewModel()
            {
                Empresas = empresas
            };

            return View(model);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItem(Empresa novaEmpresa)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var successful = await _empresaService.AddItemAsync(novaEmpresa);
            if (!successful)
            {
                return BadRequest("Não foi possível adicionar o item.");
            }
            return RedirectToAction("Index");
        }
    }
}
