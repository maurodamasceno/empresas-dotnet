﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppEmpresas.Models
{
    public class Empresa
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string UserId { get; set; }

    }
}
