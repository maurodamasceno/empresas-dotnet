﻿using Microsoft.EntityFrameworkCore;

namespace AppEmpresas.Models
{
    public class EmpresaContext : DbContext
    {
        public EmpresaContext(DbContextOptions<EmpresaContext> options) : base(options)
        {

        }
        public DbSet<Empresa> EmpresaItens { get; set; }
    }
}